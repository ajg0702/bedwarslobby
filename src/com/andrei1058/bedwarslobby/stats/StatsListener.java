package com.andrei1058.bedwarslobby.stats;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.arena.ArenaData;
import com.andrei1058.bedwarslobby.arena.ArenaGUI;
import com.andrei1058.bedwarslobby.arena.ArenaStatus;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import com.andrei1058.bedwarslobby.misc.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class StatsListener implements Listener {

    @EventHandler
    // Create cache for player if does not exist yet.
    public void onLogin(PlayerLoginEvent e) {
        final Player p = e.getPlayer();
        //create cache row for player
        StatsManager.getStatsCache().createStatsCache(p);
        //update local cache for player
        Bukkit.getScheduler().runTaskAsynchronously(SpigotMain.getInstance(), () -> SpigotMain.remoteDatabase.updateLocalCache(p.getUniqueId()));
    }


    @EventHandler
    public void onArenaSelectorClick(InventoryClickEvent e) {
        if (StatsManager.openStats.contains(e.getWhoClicked().getUniqueId())) e.setCancelled(true);
    }

    @EventHandler
    public void onArenaSelectorClose(InventoryCloseEvent e) {
        StatsManager.openStats.remove(e.getPlayer().getUniqueId());
    }
}
