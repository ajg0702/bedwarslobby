package com.andrei1058.bedwarslobby.stats;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.config.ConfigPath;
import com.andrei1058.bedwarslobby.language.Messages;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.andrei1058.bedwarslobby.language.Language.getList;
import static com.andrei1058.bedwarslobby.language.Language.getMsg;

public class StatsManager {

    private static StatsCache statsCache;

    public static LinkedList<UUID> openStats = new LinkedList<>();

    public StatsManager() {
        registerListeners();
        statsCache = new StatsCache();
    }

    /**
     * Register listeners related to stats cache.
     */
    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new StatsListener(), SpigotMain.getInstance());
    }

    /**
     * Get stats cache.
     */
    public static StatsCache getStatsCache() {
        return statsCache;
    }

    /**
     * Replace stats placeholders for a string.
     * Replace PAPI placeholders aswell.
     */
    public static String replaceStatsPlaceholders(Player pl, String s, boolean papiReplacements) {

        if (s.contains("{kills}"))
            s = s.replace("{kills}", String.valueOf(StatsManager.getStatsCache().getKills(pl.getUniqueId())));
        if (s.contains("{deaths}"))
            s = s.replace("{deaths}", String.valueOf(StatsManager.getStatsCache().getDeaths(pl.getUniqueId())));
        if (s.contains("{losses}"))
            s = s.replace("{losses}", String.valueOf(StatsManager.getStatsCache().getLosses(pl.getUniqueId())));
        if (s.contains("{wins}"))
            s = s.replace("{wins}", String.valueOf(StatsManager.getStatsCache().getWins(pl.getUniqueId())));
        if (s.contains("{finalKills}"))
            s = s.replace("{finalKills}", String.valueOf(StatsManager.getStatsCache().getFinalKills(pl.getUniqueId())));
        if (s.contains("{finalDeaths}"))
            s = s.replace("{finalDeaths}", String.valueOf(StatsManager.getStatsCache().getFinalDeaths(pl.getUniqueId())));
        if (s.contains("{bedsDestroyed}"))
            s = s.replace("{bedsDestroyed}", String.valueOf(StatsManager.getStatsCache().getBedsDestroyed(pl.getUniqueId())));
        if (s.contains("{gamesPlayed}"))
            s = s.replace("{gamesPlayed}", String.valueOf(StatsManager.getStatsCache().getGamesPlayed(pl.getUniqueId())));
        if (s.contains("{firstPlay}"))
            s = s.replace("{firstPlay}", new SimpleDateFormat(getMsg(pl, Messages.FORMATTING_STATS_DATE_FORMAT)).format(StatsManager.getStatsCache().getFirstPlay(pl.getUniqueId())));
        if (s.contains("{lastPlay}"))
            s = s.replace("{lastPlay}", new SimpleDateFormat(getMsg(pl, Messages.FORMATTING_STATS_DATE_FORMAT)).format(StatsManager.getStatsCache().getLastPlay(pl.getUniqueId())));
        if (s.contains("{player}")) s = s.replace("{player}", pl.getName());
        if (s.contains("{prefix}")) s = s.replace("{prefix}", SpigotMain.getChat().getPrefix(pl));
        if (s.contains("{suffix}")) s = s.replace("{suffix}", SpigotMain.getChat().getPrefix(pl));

        return s;
    }

    /**
     * open stats GUI to player
     */
    public static void openStatsGUI(Player p) {


        /** create inventory */
        Inventory inv = Bukkit.createInventory(null, SpigotMain.config.getInt(ConfigPath.GENERAL_CONFIGURATION_STATS_GUI_SIZE), replaceStatsPlaceholders(p, getMsg(p, Messages.PLAYER_STATS_GUI_INV_NAME), true));

        /** add custom items to gui */
        for (String s : SpigotMain.config.getYml().getConfigurationSection(ConfigPath.GENERAL_CONFIGURATION_STATS_PATH).getKeys(false)) {
            /** skip inv size, it isn't a content */
            if (ConfigPath.GENERAL_CONFIGURATION_STATS_GUI_SIZE.contains(s)) continue;
            /** create new itemStack for content */
            ItemStack i = SpigotMain.getVersionSupport().createItemStack(SpigotMain.config.getYml().getString(ConfigPath.GENERAL_CONFIGURATION_STATS_ITEMS_MATERIAL.replace("%path%", s)).toUpperCase(), 1, (short) SpigotMain.config.getInt(ConfigPath.GENERAL_CONFIGURATION_STATS_ITEMS_DATA.replace("%path%", s)));
            ItemMeta im = i.getItemMeta();
            im.setDisplayName(replaceStatsPlaceholders(p, getMsg(p, Messages.PLAYER_STATS_GUI_PATH + "-" + s + "-name"), true));
            List<String> lore = new ArrayList<>();
            for (String string : getList(p, Messages.PLAYER_STATS_GUI_PATH + "-" + s + "-lore")) {
                lore.add(replaceStatsPlaceholders(p, string, true));
            }
            im.setLore(lore);
            i.setItemMeta(im);
            inv.setItem(SpigotMain.config.getInt(ConfigPath.GENERAL_CONFIGURATION_STATS_ITEMS_SLOT.replace("%path%", s)), i);
        }

        openStats.add(p.getUniqueId());
        p.openInventory(inv);
    }
}
