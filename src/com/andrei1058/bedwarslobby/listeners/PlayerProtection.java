package com.andrei1058.bedwarslobby.listeners;

import com.andrei1058.bedwarslobby.SpigotMain;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerProtection implements Listener {

    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDrink(PlayerItemConsumeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDamageByEntity(EntityDamageByEntityEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        e.setDeathMessage(null);
        e.getEntity().spigot().respawn();
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        if (SpigotMain.config.getString("spawn-location").isEmpty() && SpigotMain.config.getString("spawn-location").equals("null"))
        e.setRespawnLocation(SpigotMain.config.getConfigLoc("spawn-location"));
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (e.getTo().getY() < 0) {
            try {
                e.getPlayer().teleport(SpigotMain.config.getConfigLoc("spawn-location"));
            } catch (Exception ex){}
        }
    }

    @EventHandler
    public void onProjHit(ProjectileHitEvent e) {
        e.getEntity().remove();
    }

    @EventHandler
    public void onItemFrameDamage(EntityDamageByEntityEvent  e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent e) {
        e.getDrops().clear();
    }

}
