package com.andrei1058.bedwarslobby.listeners;


import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import com.andrei1058.bedwarslobby.papi.SupportPAPI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.Iterator;

public class PlayerChat implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        if (e.isCancelled()) return;
        if (p.hasPermission("bw.chatcolor") || p.hasPermission("bw.*") || p.hasPermission("bw.vip")) {
            e.setMessage(ChatColor.translateAlternateColorCodes('&', e.getMessage()));
        }
        e.setFormat(SupportPAPI.getSupportPAPI().replace(e.getPlayer(), Language.getMsg(p, Messages.FORMATTING_CHAT_LOBBY).replace("{vPrefix}", SpigotMain.getChat().getPrefix(p)).replace("{vSuffix}", SpigotMain.getChat().getSuffix(p))
                .replace("{player}", p.getName()).replace("{level}", SpigotMain.getLevelSupport().getLevel(p))).replace("{message}", "%2$s"));

    }
}
