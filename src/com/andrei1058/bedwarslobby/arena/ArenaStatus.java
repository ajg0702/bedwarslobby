package com.andrei1058.bedwarslobby.arena;

public enum ArenaStatus {

    WAITING,
    STARTING,
    PLAYING,
    RESTARTING,
    OFFLINE
}
