package com.andrei1058.bedwarslobby;

import com.andrei1058.bedwarslobby.arena.ArenaData;
import com.andrei1058.bedwarslobby.citizens.CitizensListener;
import com.andrei1058.bedwarslobby.citizens.JoinNPC;
import com.andrei1058.bedwarslobby.commads.main.MainCommand;
import com.andrei1058.bedwarslobby.commads.party.PartyCommand;
import com.andrei1058.bedwarslobby.config.ConfigPath;
import com.andrei1058.bedwarslobby.config.Configuration;
import com.andrei1058.bedwarslobby.database.Database;
import com.andrei1058.bedwarslobby.database.MySQL;
import com.andrei1058.bedwarslobby.database.SQLite;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.leaderheads.LeaderHeadsSupport;
import com.andrei1058.bedwarslobby.levels.Level;
import com.andrei1058.bedwarslobby.levels.internal.InternalLevel;
import com.andrei1058.bedwarslobby.levels.internal.LevelListeners;
import com.andrei1058.bedwarslobby.listeners.*;
import com.andrei1058.bedwarslobby.misc.Metrics;
import com.andrei1058.bedwarslobby.misc.Updater;
import com.andrei1058.bedwarslobby.misc.Utils;
import com.andrei1058.bedwarslobby.papi.PAPISupport;
import com.andrei1058.bedwarslobby.papi.SupportPAPI;
import com.andrei1058.bedwarslobby.party.Internal;
import com.andrei1058.bedwarslobby.party.Parties;
import com.andrei1058.bedwarslobby.party.Party;
import com.andrei1058.bedwarslobby.scoreboard.Refresh;
import com.andrei1058.bedwarslobby.stats.StatsManager;
import com.andrei1058.bedwarslobby.task.DeadArenaCheckTask;
import com.andrei1058.bedwarslobby.task.LobbySocket;
import com.andrei1058.bedwarslobby.vault.*;
import com.andrei1058.bedwarslobby.versionsupport.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class SpigotMain extends JavaPlugin {

    private static SpigotMain instance;

    private static VersionSupport versionSupport;

    public static Configuration config;
    public static Database remoteDatabase;

    private static Chat chat;
    private static Economy economy;
    private static Level levelManager;
    private static Party party;

    private boolean support = true;
    private String version = Bukkit.getServer().getClass().getName().split("\\.")[3];

    public void onLoad() {

        //Don not enable near BedWars1058
        if (Bukkit.getPluginManager().getPlugin("BedWars1058") != null) {
            this.setEnabled(false);
            this.getLogger().severe("This plugin can't run near BedWars1058!");
            support = true;
            return;
        }

        instance = this;
        //create messages
        Language en = new Language("en");
        Language.setupLang(en);
        Language.getLanguages().remove(en);

        switch (version) {
            case "v1_8_R3":
                versionSupport = new v1_8_R3();
                break;
            case "v1_9_R1":
                versionSupport = new v1_9_R1();
                break;
            case "v1_9_R2":
                versionSupport = new v1_9_R2();
                break;
            case "v1_10_R1":
                versionSupport = new v1_10_R1();
                break;
            case "v1_11_R1":
                versionSupport = new v1_11_R1();
                break;
            case "v1_12_R1":
                versionSupport = new v1_12_R1();
                break;
            case "v1_13_R1":
                versionSupport = new v1_13_R1();
                break;
            case "v1_13_R2":
                versionSupport = new v1_13_R2();
                break;
            case "v1_14_R1":
                versionSupport = new v1_14_R1();
                break;
            default:
                support = false;
        }
    }

    public void onEnable() {

        if (!support) {
            //Don not enable near BedWars1058
            if (Bukkit.getPluginManager().getPlugin("BedWars1058") != null) {
                Bukkit.getPluginManager().disablePlugin(this);
                this.getLogger().severe("This plugin can't run near BedWars1058!");
                return;
            }
            Bukkit.getPluginManager().disablePlugin(this);
            this.getLogger().severe("I can't run on your version: " + version);
            return;
        } else {
            this.getLogger().info("Loading support for paper/ spigot " + version + ".");
        }

        config = new Configuration("config", "plugins/" + getName());

        Bukkit.getScheduler().runTaskLater(this, () -> {
            try {
                new LobbySocket(config.getInt("socket-port"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            Updater.checkUpdate(this);
        }, 100L);

        Bukkit.getMessenger().registerOutgoingPluginChannel(getInstance(), "BungeeCord");
        Bukkit.getPluginManager().registerEvents(new JoinOptions(), this);
        Bukkit.getPluginManager().registerEvents(new CommandItems(), this);
        Bukkit.getPluginManager().registerEvents(new ArenaSelectorListener(), this);
        Bukkit.getPluginManager().registerEvents(new JoinSigns(), this);

        if (config.getBoolean("protect-world")) Bukkit.getPluginManager().registerEvents(new WorldProtection(), this);
        if (config.getBoolean("protect-players"))
            Bukkit.getPluginManager().registerEvents(new PlayerProtection(), this);

        //Register commands
        getVersionSupport().registerCommand(new MainCommand("bw"));
        getVersionSupport().registerCommand(new PartyCommand("party"));

        if (config.getConfigLoc("spawn-location") != null) {
            Bukkit.getScheduler().runTaskLater(this, () -> config.getConfigLoc("spawn-location").getWorld().getEntities().stream().filter(e -> e instanceof Monster).forEach(Entity::remove), 20L);
        }

        if (SpigotMain.config.getYml().get("join-signs.locations") != null) {
            for (String st : SpigotMain.config.getYml().getStringList("join-signs.locations")) {
                String[] d = st.split(",");
                if (ArenaData.getArenaByBungeeName(d[0]) == null) {
                    World w = Bukkit.getWorld(d[6]);
                    if (w == null) continue;
                    Block block = w.getBlockAt((int) Double.parseDouble(d[1]), (int) Double.parseDouble(d[2]), (int) Double.parseDouble(d[3]));
                    if (block == null) continue;
                    if (block.getType() == Material.SIGN || block.getType() == Material.WALL_SIGN) {
                        Sign s = (Sign) block.getState();
                        Utils.setOfflineSign(s, d[0]);
                    }
                }
            }
        }

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new DeadArenaCheckTask(), 20L, 40L);

        // Database support
        MySQL mySQL = new MySQL();
        Long time = System.currentTimeMillis();
        if (!mySQL.connect()) {
            this.getLogger().severe("Could not connect to database! Please verify your credentials and make sure that the server IP is whitelisted in MySQL.");
            remoteDatabase = new SQLite();
        } else {
            remoteDatabase = mySQL;
        }
        if (System.currentTimeMillis() - time >= 5000) {
            this.getLogger().severe("It took " + ((System.currentTimeMillis() - time) / 1000) + " ms to establish a database connection!\n" +
                    "Using this remote connection is not recommended!");
        }
        remoteDatabase.init();

        //Init stats manager
        new StatsManager();

        //Load languages and set the default language
        String whatLang = "en";
        for (File f : Objects.requireNonNull(new File("plugins/" + this.getDescription().getName() + "/Languages").listFiles())) {
            if (f.isFile()) {
                if (f.getName().contains("messages_") && f.getName().contains(".yml")) {
                    String lang = f.getName().replace("messages_", "").replace(".yml", "");
                    if (lang.equalsIgnoreCase(config.getString("default-language"))) {
                        whatLang = f.getName().replace("messages_", "").replace(".yml", "");
                    }
                    Language.setupLang(new Language(lang));
                }
            }
        }
        Language.setDefaultLanguage(Language.getLang(whatLang));

        //Add default messages for new custom join items
        for (Language l : Language.getLanguages()) {
            Language.addDefaultMessagesCommandItems(l);
        }
        //Create messages for stats gui items if new items are added, for each language
        Language.setupCustomStatsMessages();

        //remove languages if disabled
        //default language cannot be disabled
        for (String iso : config.getYml().getStringList(ConfigPath.GENERAL_CONFIGURATION_DISABLED_LANGUAGES)) {
            Language l = Language.getLang(iso);
            if (l != null) {
                if (l != Language.defaultLanguage) Language.getLanguages().remove(l);
            }
        }

        //disable name spaced commands
        Bukkit.spigot().getConfig().set("commands.send-namespaced", false);
        try {
            Bukkit.spigot().getConfig().save("spigot.yml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Vault support
        if (this.getServer().getPluginManager().getPlugin("Vault") != null) {
            try {
                RegisteredServiceProvider rsp = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
                WithChat.setChat((net.milkbowl.vault.chat.Chat) rsp.getProvider());
                getLogger().info("Hook into vault chat support!");
                chat = new WithChat();
            } catch (Exception var2_2) {
                chat = new NoChat();
            }
            try {
                RegisteredServiceProvider rsp = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
                WithEconomy.setEconomy((net.milkbowl.vault.economy.Economy) rsp.getProvider());
                getLogger().info("Hook into vault economy support!");
                economy = new WithEconomy();
            } catch (Exception var2_2) {
                economy = new NoEconomy();
            }
        } else {
            chat = new NoChat();
            economy = new NoEconomy();
        }

        //Check if should format chat
        if (config.getBoolean("format-chat")) {
            Bukkit.getPluginManager().registerEvents(new PlayerChat(), this);
        }

        //PlaceholderAPI Support
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            getLogger().info("Hook into PlaceholderAPI support!");
            new PAPISupport().register();
            SupportPAPI.setSupportPAPI(new SupportPAPI.withPAPI());
        }

        //Levels support
        setLevelAdapter(new InternalLevel());

        //Scoreboard refresh task
        Bukkit.getScheduler().runTaskTimer(this, new Refresh(), 100L, 30L);

        //Party support
        if (config.getYml().getBoolean(ConfigPath.GENERAL_CONFIGURATION_ALLOW_PARTIES)) {
            if (Bukkit.getPluginManager().getPlugin("Parties") != null) {
                if (getServer().getPluginManager().getPlugin("Parties").isEnabled()) {
                    getLogger().info("Hook into Parties (by AlessioDP) support!");
                    party = new Parties();
                }
            }
        }
        if (party == null) {
            party = new Internal();
            getLogger().info("Loading internal Party system. /party");
        }

        //Citizens support
        /* Citizens support */
        if (this.getServer().getPluginManager().getPlugin("Citizens") != null) {
            if (Bukkit.getPluginManager().getPlugin("Citizens").isEnabled()) {
                JoinNPC.setCitizensSupport(true);
                getLogger().info("Hook into Citizens support. /bw npc");
                Bukkit.getPluginManager().registerEvents(new CitizensListener(), this);
                Bukkit.getScheduler().runTaskLater(this, () -> {
                    //spawn NPCs
                    try {
                        JoinNPC.spawnNPCs();
                    } catch (Exception e) {
                        this.getLogger().severe("Could not spawn CmdJoin NPCs. Make sure you have right version of Citizens for your server!");
                        JoinNPC.setCitizensSupport(false);
                    }
                }, 40L);
            }
        }

        //LeaderHeads Support
        LeaderHeadsSupport.initLeaderHeads();

        //Metrics
        Metrics metrics = new Metrics(this);
        metrics.addCustomChart(new Metrics.SimplePie("language", () -> Language.defaultLanguage.getIso()));
    }

    @Override
    public void onDisable() {
        LobbySocket.run = false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return super.onTabComplete(sender, command, alias, args);
    }

    public Configuration getConfiguration() {
        return config;
    }

    public static SpigotMain getInstance() {
        return instance;
    }

    public static VersionSupport getVersionSupport() {
        return versionSupport;
    }

    /**
     * Get rank prefix support.
     */
    public static Chat getChat() {
        return chat;
    }

    /**
     * Get economy support.
     */
    public static Economy getEconomy() {
        return economy;
    }

    /**
     * Get current levels manager.
     */
    public static Level getLevelSupport() {
        return levelManager;
    }

    /**
     * Set the levels manager.
     * You can use this to add your own levels manager just implement
     * the LevelCMD interface so the plugin will be able to display
     * the level internally.
     */
    public static void setLevelAdapter(Level levelsManager) {
        if (levelsManager instanceof InternalLevel) {
            if (LevelListeners.instance == null) {
                Bukkit.getPluginManager().registerEvents(new LevelListeners(), getInstance());
            }
        } else {
            if (LevelListeners.instance != null) {
                PlayerJoinEvent.getHandlerList().unregister(LevelListeners.instance);
                PlayerQuitEvent.getHandlerList().unregister(LevelListeners.instance);
                LevelListeners.instance = null;
            }
        }
        levelManager = levelsManager;
    }

    /**
     * Get party support.
     */
    public static Party getParty() {
        return party;
    }
}
