package com.andrei1058.bedwarslobby.commads.main;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.misc.Utils;
import com.andrei1058.bedwarslobby.arena.ArenaData;
import com.andrei1058.bedwarslobby.citizens.JoinNPC;
import com.andrei1058.bedwarslobby.commads.SubCommand;
import com.andrei1058.bedwarslobby.config.ConfigPath;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import com.google.common.base.Joiner;
import net.citizensnpcs.api.CitizensAPI;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NPCCMD extends SubCommand {

    //main usage
    private final List<BaseComponent> MAIN_USAGE = Arrays.asList(Utils.msgHoverClick("§f\n§c▪ §7Usage: §e/bw " + getName() + " add", "§fUse this command to create a join NPC.\n§fClick to see the syntax.", "/bw "+getName()+" add", ClickEvent.Action.RUN_COMMAND),
            Utils.msgHoverClick("§c▪ §7Usage: §e/bw " + getName() + " remove", "§fStay in front of a NPC in order to remove it.", "/bw "+getName()+" remove", ClickEvent.Action.SUGGEST_COMMAND));
    private final List<BaseComponent> ADD_USAGE = Arrays.asList(Utils.msgHoverClick("f\n§c▪ §7Usage: §e§o/bw " + getName() + " add <skin> <arenaGroup> <§7line1§9\\n§7line2§e>\n§7You can use §e{players} §7for the players count in this arena §7group.", "Click to use.", "/bw "+getName()+" add", ClickEvent.Action.SUGGEST_COMMAND));
    private final String NO_GROUP = "§c▪ §bCound not find any arena with following group: §c%name%\n§c▪ §bUse \"default\" for all groups.";
    private final String NPC_SET = "§a§c▪ §bNPC: %name% §bwas set!";
    private final String NO_NPCS = "§c▪ §bThere isn't any NPC nearby.";
    private final String NO_SET = "§c▪ §bThere isn't any NPC set yet!";
    private final String NPC_REMOVED = "§c▪ §bThe target NPC was removed!";

    public NPCCMD(String name, String permission) {
        super(name, permission);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (s instanceof ConsoleCommandSender) return;
        if (!JoinNPC.isCitizensSupport()) return;

        Player p = (Player) s;
        if (!hasPermission(p)){
            p.sendMessage(Language.getMsg(p, Messages.COMMAND_NOT_FOUND_OR_INSUFF_PERMS));
            return;
        }

        if (args.length < 1) {
            for (BaseComponent bc : MAIN_USAGE){
                p.spigot().sendMessage(bc);
            }
            return;
        }
        if (args[0].equalsIgnoreCase("add")) {
            if (args.length < 4) {
                for (BaseComponent bc : ADD_USAGE){
                    p.spigot().sendMessage(bc);
                }
                return;
            }

            boolean found = false;
            if (args[2].equalsIgnoreCase("default")){
                found = true;
            } else {
                for (ArenaData ad : ArenaData.getArenaByIdentifier().values()){
                    if (ad.getArenaGroup().equalsIgnoreCase(args[2])){
                        found = true;
                        break;
                    }
                }
            }


            if (!found) {
                if (!args[2].equalsIgnoreCase("default")) {
                    p.sendMessage(NO_GROUP.replace("%name%", args[2]));
                    return;
                }
            }

            List<String> npcs;
            if (SpigotMain.config.getYml().get(ConfigPath.GENERAL_CONFIGURATION_NPC_LOC_STORAGE) != null) {
                npcs = SpigotMain.config.getYml().getStringList(ConfigPath.GENERAL_CONFIGURATION_NPC_LOC_STORAGE);
            } else {
                npcs = new ArrayList<>();
            }
            String name = Joiner.on(" ").join(args).replace(args[0] + " " + args[1] + " " + args[2] + " ", "");
            net.citizensnpcs.api.npc.NPC npc = JoinNPC.spawnNPC(p.getLocation(), name, args[2], args[1], null);
            npcs.add(SpigotMain.config.getConfigLoc(p.getLocation()) + "," + args[1] + "," + name + "," + args[2] + "," + npc.getId());
            p.sendMessage(NPC_SET.replace("%name%", name.replace("&", "§").replace("\\\\n", " ")));
            SpigotMain.config.set(ConfigPath.GENERAL_CONFIGURATION_NPC_LOC_STORAGE, npcs);

        } else if (args[0].equalsIgnoreCase("remove")) {

            List<Entity> e = p.getNearbyEntities(4, 4, 4);
            if (e.isEmpty()) {
                p.sendMessage(NO_NPCS);
                return;
            }
            if (SpigotMain.config.getYml().get(ConfigPath.GENERAL_CONFIGURATION_NPC_LOC_STORAGE) == null) {
                p.sendMessage(NO_SET);
                return;
            }
            net.citizensnpcs.api.npc.NPC npc = getTarget(p);
            if (npc == null) {
                p.sendMessage(NO_NPCS);
                return;
            }
            List<String> locations = SpigotMain.config.getYml().getStringList(ConfigPath.GENERAL_CONFIGURATION_NPC_LOC_STORAGE);
            for (Integer id : JoinNPC.npcs.keySet()) {
                if (id == npc.getId()) {
                    for (String loc : SpigotMain.config.getYml().getStringList(ConfigPath.GENERAL_CONFIGURATION_NPC_LOC_STORAGE)) {
                        locations.remove(loc);
                        break;
                    }
                }
            }
            JoinNPC.npcs.remove(npc.getId());
            for (Entity e2 : npc.getEntity().getNearbyEntities(0, 3, 0)) {
                if (e2.getType() == EntityType.ARMOR_STAND) {
                    e2.remove();
                }
            }
            SpigotMain.config.set(ConfigPath.GENERAL_CONFIGURATION_NPC_LOC_STORAGE, locations);
            npc.destroy();
            p.sendMessage(NPC_REMOVED);
        } else {
            for (BaseComponent bc : MAIN_USAGE){
                p.spigot().sendMessage(bc);
            }
        }
        return;
    }

    @Override
    public List<String> tabComplete(CommandSender s, String alias, String[] args, Location location) {
        return Arrays.asList("remove", "add");
    }


    public static ArmorStand createArmorStand(Location loc) {
        ArmorStand a = loc.getWorld().spawn(loc, ArmorStand.class);
        a.setGravity(false);
        a.setVisible(false);
        a.setCustomNameVisible(false);
        a.setMarker(true);
        return a;
    }

    /**
     * Get target NPC
     */
    public static net.citizensnpcs.api.npc.NPC getTarget(final Player player) {

        BlockIterator iterator = new BlockIterator(player.getWorld(), player.getLocation().toVector(), player.getEyeLocation().getDirection(), 0, 100);
        while (iterator.hasNext()) {
            Block item = iterator.next();
            for (Entity entity : player.getNearbyEntities(100, 100, 100)) {
                int acc = 2;
                for (int x = -acc; x < acc; x++) {
                    for (int z = -acc; z < acc; z++) {
                        for (int y = -acc; y < acc; y++) {
                            if (entity.getLocation().getBlock().getRelative(x, y, z).equals(item)) {
                                if (entity.hasMetadata("NPC")) {
                                    net.citizensnpcs.api.npc.NPC npc = CitizensAPI.getNPCRegistry().getNPC(entity);
                                    if (npc != null) return npc;
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
}
