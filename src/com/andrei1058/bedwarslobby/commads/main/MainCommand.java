package com.andrei1058.bedwarslobby.commads.main;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.misc.Utils;
import com.andrei1058.bedwarslobby.commads.ParentCommand;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class MainCommand extends ParentCommand {

    private static MainCommand instance;

    /**
     * Create a new Parent Command
     *
     * @param name
     */
    public MainCommand(String name) {
        super(name);
        instance = this;
        setAliases(Arrays.asList("bedwars"));
        addSubCommand(new BuildCMD("build", "bw.staff"));
        addSubCommand(new SetLobbyCMD("setspawn", "bw.staff"));
        addSubCommand(new SelectorCMD("gui", ""));
        addSubCommand(new StatsCMD("stats", ""));
        addSubCommand(new LevelCMD("level", "bw.level"));
        addSubCommand(new LangCMD("lang", ""));
        addSubCommand(new LangCMD("language", ""));
        addSubCommand(new JoinCMD("join", ""));
    }

    @Override
    public void sendDefaultMessage(CommandSender s) {
        if (s instanceof ConsoleCommandSender) return;
        Player p = (Player) s;

        s.sendMessage(" ");
        s.sendMessage("§8§l|-" + " §6" + SpigotMain.getInstance().getDescription().getName() + " v" + SpigotMain.getInstance().getDescription().getVersion() + " §7- §cCommands");
        s.sendMessage(" ");
        if (hasSubCommand("setspawn") && getSubCommand("setspawn").hasPermission(s)) {
            p.spigot().sendMessage(Utils.createTC(Language.getMsg(p, Messages.COMMAND_SET_SPAWN_DISPLAY), "/bw setspawn", Language.getMsg(p, Messages.COMMAND_SET_SPAWN_HOVER)));
        }
        if (hasSubCommand("build") && getSubCommand("build").hasPermission(s)) {
            p.spigot().sendMessage(Utils.createTC(Language.getMsg(p, Messages.COMMAND_BUILD_DISPLAY), "/bw build", Language.getMsg(p, Messages.COMMAND_BUILD_HOVER)));
        }
        if (hasSubCommand("level") && hasSubCommand("level")) {
            p.spigot().sendMessage(Utils.createTC(Language.getMsg(p, Messages.COMMAND_LEVEL_DISPLAY), "/bw level", Language.getMsg(p, Messages.COMMAND_LEVEL_HOVER)));
        }
        if (hasSubCommand("gui")) {
            p.spigot().sendMessage(Utils.createTC(Language.getMsg(p, Messages.COMMAND_GUI_DISPLAY), "/bw gui", Language.getMsg(p, Messages.COMMAND_GUI_HOVER)));
        }
        if (hasSubCommand("stats")) {
            p.spigot().sendMessage(Utils.createTC(Language.getMsg(p, Messages.COMMAND_STATS_DISPLAY), "/bw stats", Language.getMsg(p, Messages.COMMAND_STATS_HOVER)));
        }
        if (hasSubCommand("lang")) {
            p.spigot().sendMessage(Utils.createTC(Language.getMsg(p, Messages.COMMAND_LANGUAGE_DISPLAY), "/bw lang", Language.getMsg(p, Messages.COMMAND_LANGUAGE_HOVER)));
        }
    }

    public static MainCommand getInstance() {
        return instance;
    }
}
