package com.andrei1058.bedwarslobby.misc;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.arena.ArenaData;
import com.andrei1058.bedwarslobby.config.ConfigPath;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.andrei1058.bedwarslobby.SpigotMain.getParty;
import static com.andrei1058.bedwarslobby.language.Language.getMsg;


public class Utils {

    /**
     * Check if given string is an integer.
     */
    public static boolean isInteger(String string) {
        try {
            Integer.parseInt(string);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public static void sendJoinItems(Player p) {
        if (SpigotMain.config.getYml().get("join-items") == null) return;
        p.getInventory().clear();

        for (String item : SpigotMain.config.getYml().getConfigurationSection("join-items").getKeys(false)) {
            if (SpigotMain.config.getYml().get(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_MATERIAL.replace("%path%", item)) == null) {
                SpigotMain.getInstance().getLogger().severe(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_MATERIAL.replace("%path%", item) + " is not set!");
                continue;
            }
            if (SpigotMain.config.getYml().get(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_DATA.replace("%path%", item)) == null) {
                SpigotMain.getInstance().getLogger().severe(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_DATA.replace("%path%", item) + " is not set!");
                continue;
            }
            if (SpigotMain.config.getYml().get(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_SLOT.replace("%path%", item)) == null) {
                SpigotMain.getInstance().getLogger().severe(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_SLOT.replace("%path%", item) + " is not set!");
                continue;
            }
            if (SpigotMain.config.getYml().get(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_ENCHANTED.replace("%path%", item)) == null) {
                SpigotMain.getInstance().getLogger().severe(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_ENCHANTED.replace("%path%", item) + " is not set!");
                continue;
            }
            if (SpigotMain.config.getYml().get(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_COMMAND.replace("%path%", item)) == null) {
                SpigotMain.getInstance().getLogger().severe(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_COMMAND.replace("%path%", item) + " is not set!");
                continue;
            }
            ItemStack i = createItem(Material.valueOf(SpigotMain.config.getYml().getString(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_MATERIAL.replace("%path%", item))),
                    (byte) SpigotMain.config.getInt(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_DATA.replace("%path%", item)),
                    SpigotMain.config.getBoolean(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_ENCHANTED.replace("%path%", item)),
                    getMsg(p, Messages.GENERAL_CONFIGURATION_LOBBY_ITEMS_NAME.replace("%path%", item)),
                    Language.getList(p, Messages.GENERAL_CONFIGURATION_LOBBY_ITEMS_LORE.replace("%path%", item)),
                    p, "RUNCOMMAND", SpigotMain.config.getYml().getString(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_COMMAND.replace("%path%", item)));

            p.getInventory().setItem(SpigotMain.config.getInt(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_SLOT.replace("%path%", item)), i);
        }
    }

    /**
     * Create an item stack
     *
     * @param material item material
     * @param data     item data
     * @param name     item name
     * @param lore     item lore
     * @param owner    in case of skull, can be null, don't worry
     */
    public static ItemStack createItem(Material material, byte data, boolean enchanted, String name, List<String> lore, Player owner, String metaKey, String metaData) {
        ItemStack i = new ItemStack(material, 1, data);
        if (owner != null) {
            if (SpigotMain.getVersionSupport().isPlayerHead(i)) {
                SkullMeta sm = (SkullMeta) i.getItemMeta();
                sm.setOwner(owner.getName());
                i.setItemMeta(sm);
            }
        }
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(name);
        im.setLore(lore);
        if (enchanted) {
            im.addEnchant(Enchantment.LUCK, 1, true);
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        i.setItemMeta(im);
        if (!(metaData.isEmpty() || metaKey.isEmpty())) {
            i = SpigotMain.getVersionSupport().addCustomData(i, metaKey + "_" + metaData);
        }
        return i;
    }

    /**
     * Create a text component.
     */
    public static TextComponent createTC(String text, String suggest, String shot_text) {
        TextComponent tx = new TextComponent(text);
        tx.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, suggest));
        tx.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(shot_text).create()));
        return tx;
    }

    /**
     * Send player to a server.
     */
    public static void sendPlayerToServer(Player p, ArenaData arenaData, boolean skipOwnerCheck) {
        if (!skipOwnerCheck) {
            if (getParty().hasParty(p)) {
                if (!getParty().isOwner(p)) {
                    p.sendMessage(getMsg(p, Messages.COMMAND_JOIN_DENIED_NOT_PARTY_LEADER));
                    return;
                }
                if (getParty().partySize(p) > arenaData.getMaxInTeam() * arenaData.getMaxPlayers() / arenaData.getMaxInTeam() - arenaData.getCurrentPlayers()) {
                    p.sendMessage(getMsg(p, Messages.COMMAND_JOIN_DENIED_PARTY_TOO_BIG));
                    return;
                }
                for (Player mem : getParty().getMembers(p)) {
                    if (mem == p) continue;
                    sendPlayerToServer(mem, arenaData, true);
                }
            }
        }
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(arenaData.getBungeeName());
        p.sendPluginMessage(SpigotMain.getInstance(), "BungeeCord", out.toByteArray());
    }

    /**
     * Create offline status sign.
     */
    public static void setOfflineSign(Sign s, String arena) {
        int line = 0;
        for (String string : SpigotMain.config.l("join-signs.format.offline")) {
            s.setLine(line, string.replace("[on]", "0").replace("[max]",
                    "0").replace("[arena]", arena).replace("[status]",
                    SpigotMain.config.m("messages.arena-status.offline")));
            line++;
        }

        try {
            s.update(true);
        } catch (Exception e) {
        }

        String path = "join-signs.status-block.offline.material",
                data = "join-signs.status-block.offline.data";

        Bukkit.getScheduler().runTask(SpigotMain.getInstance(), () ->
        {
            s.getLocation().getBlock().getRelative(((org.bukkit.material.Sign) s.getData()).getAttachedFace()).setType(Material.valueOf(SpigotMain.config.getString(path)));
            SpigotMain.getVersionSupport().setBlockData(s.getBlock().getRelative(((org.bukkit.material.Sign) s.getData()).getAttachedFace()), (byte) SpigotMain.config.getInt(data));
            s.update(true);
        });
    }

    /**
     * Create TextComponent message.
     */
    public static TextComponent msgHoverClick(String msg, String hover, String click, ClickEvent.Action clickAction) {
        TextComponent tc = new TextComponent(msg);
        tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hover).create()));
        tc.setClickEvent(new ClickEvent(clickAction, click));
        return tc;
    }

    /**
     * Check if is a number.
     */
    public static boolean isNumber(String s) {
        try {
            Double.parseDouble(s);
        } catch (Exception e) {
            try {
                Integer.parseInt(s);
            } catch (Exception ex) {
                try {
                    Long.parseLong(s);
                } catch (Exception exx) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Add a player to the most filled arena from a group.
     */
    public static boolean joinRandomFromGroup(Player p, String group) {

        List<ArenaData> arenaList = new ArrayList<>(ArenaData.getArenaByIdentifier().values());
        Collections.sort(arenaList);

        int amount = getParty().hasParty(p) ? getParty().getMembers(p).size() : 1;
        for (ArenaData a : arenaList) {
            if (!a.getArenaGroup().equalsIgnoreCase(group)) continue;
            if (a.getCurrentPlayers() == a.getMaxPlayers()) continue;
            if (a.getMaxPlayers() - a.getCurrentPlayers() >= amount) {
                Utils.sendPlayerToServer(p, a, false);
                break;
            }
        }

        return true;
    }

    /**
     * Add a player to the most filled arena.
     * Check if is the party owner first.
     */
    public static boolean joinRandomArena(Player p) {
        List<ArenaData> arenas = new ArrayList<>(ArenaData.getArenaByIdentifier().values());
        Collections.sort(arenas);
        int amount = getParty().hasParty(p) ? getParty().getMembers(p).size() : 1;

        for (ArenaData a : arenas) {
            if (a.getCurrentPlayers() == a.getMaxPlayers()) continue;
            if (a.getMaxPlayers() - a.getCurrentPlayers() >= amount) {
                sendPlayerToServer(p, a, false);
                break;
            }
        }
        return true;
    }
}
