package com.andrei1058.bedwarslobby.versionsupport;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class VersionSupport {

    public String getForCurrentVersion(String v188, String v112, String v113) {
        return v188;
    }

    public abstract ItemStack addCustomData(ItemStack i, String data);

    @SuppressWarnings("deprecation")
    public boolean isPlayerHead(ItemStack i) {
        return i.getType().toString().equals("SKULL_ITEM") && i.getData().getData() == 3;
    }

    public ItemStack getItemInHand(Player player) {
        return player.getInventory().getItemInMainHand();
    }

    public abstract boolean isCustomItem(ItemStack i);

    public abstract String getCustomData(ItemStack i);

    public abstract void registerCommand(Command command);

    public ItemStack createItemStack(String material, int amount, short data) {
        return new ItemStack(Material.valueOf(material), amount, data);
    }

    @SuppressWarnings("deprecation")
    public void setBlockData(Block block, byte data) {
        block.setData(data, true);
    }
}
