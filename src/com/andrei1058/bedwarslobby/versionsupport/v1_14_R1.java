package com.andrei1058.bedwarslobby.versionsupport;

import net.minecraft.server.v1_14_R1.ItemStack;
import net.minecraft.server.v1_14_R1.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.craftbukkit.v1_14_R1.CraftServer;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;

public class v1_14_R1 extends VersionSupport{


    @Override
    public String getForCurrentVersion(String v188, String v112, String v113){
        return v113;
    }

    @Override
    public org.bukkit.inventory.ItemStack addCustomData(org.bukkit.inventory.ItemStack i, String data) {
        ItemStack itemStack = CraftItemStack.asNMSCopy(i);
        NBTTagCompound tag = itemStack.getTag();
        if (tag == null) {
            tag = new NBTTagCompound();
            itemStack.setTag(tag);
        }

        tag.setString("BedWarsProxy", data);
        return CraftItemStack.asBukkitCopy(itemStack);
    }

    @Override
    public boolean isPlayerHead(org.bukkit.inventory.ItemStack i){
        return i.getType().toString().equals("PLAYER_HEAD");
    }

    @Override
    public boolean isCustomItem(org.bukkit.inventory.ItemStack i) {
        ItemStack itemStack = CraftItemStack.asNMSCopy(i);
        if (itemStack == null) return false;
        NBTTagCompound tag = itemStack.getTag();
        if (tag == null) return false;
        return tag.hasKey("BedWarsProxy");
    }

    @Override
    public String getCustomData(org.bukkit.inventory.ItemStack i) {
        ItemStack itemStack = CraftItemStack.asNMSCopy(i);
        NBTTagCompound tag = itemStack.getTag();
        if (tag == null) return "";
        return tag.getString("BedWarsProxy");
    }

    @Override
    public void registerCommand(Command command) {
        ((CraftServer) Bukkit.getServer()).getCommandMap().register(command.getName(), command);
    }

    @Override
    public org.bukkit.inventory.ItemStack createItemStack(String material, int amount, short data){
        return new org.bukkit.inventory.ItemStack(Material.valueOf(material), amount);
    }

    @Override
    public void setBlockData(Block block, byte data) {

    }
}
