package com.andrei1058.bedwarslobby.versionsupport;

import net.minecraft.server.v1_9_R1.ItemStack;
import net.minecraft.server.v1_9_R1.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.craftbukkit.v1_9_R1.CraftServer;
import org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack;

public class v1_9_R1 extends VersionSupport {

    @Override
    public org.bukkit.inventory.ItemStack addCustomData(org.bukkit.inventory.ItemStack i, String data) {
        ItemStack itemStack = CraftItemStack.asNMSCopy(i);
        NBTTagCompound tag = itemStack.getTag();
        if (tag == null) {
            tag = new NBTTagCompound();
            itemStack.setTag(tag);
        }

        tag.setString("BedWarsProxy", data);
        return CraftItemStack.asBukkitCopy(itemStack);
    }

    @Override
    public boolean isCustomItem(org.bukkit.inventory.ItemStack i) {
        net.minecraft.server.v1_9_R1.ItemStack itemStack = CraftItemStack.asNMSCopy(i);
        if (itemStack == null) return false;
        NBTTagCompound tag = itemStack.getTag();
        if (tag == null) return false;
        return tag.hasKey("BedWarsProxy");
    }

    @Override
    public String getCustomData(org.bukkit.inventory.ItemStack i) {
        net.minecraft.server.v1_9_R1.ItemStack itemStack = CraftItemStack.asNMSCopy(i);
        NBTTagCompound tag = itemStack.getTag();
        if (tag == null) return "";
        return tag.getString("BedWarsProxy");
    }

    @Override
    public void registerCommand(Command command) {
        ((CraftServer) Bukkit.getServer()).getCommandMap().register(command.getName(), command);
    }
}
