package com.andrei1058.bedwarslobby.versionsupport;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class v1_8_R3 extends VersionSupport {

    @Override
    public ItemStack addCustomData(org.bukkit.inventory.ItemStack i, String data) {
        net.minecraft.server.v1_8_R3.ItemStack itemStack = CraftItemStack.asNMSCopy(i);
        NBTTagCompound tag = itemStack.getTag();
        if (tag == null) {
            tag = new NBTTagCompound();
            itemStack.setTag(tag);
        }

        tag.setString("BedWarsProxy", data);
        return CraftItemStack.asBukkitCopy(itemStack);
    }

    @Override
    @SuppressWarnings("deprecation")
    public ItemStack getItemInHand(Player player){
        return player.getItemInHand();
    }

    @Override
    public boolean isCustomItem(ItemStack i) {
        net.minecraft.server.v1_8_R3.ItemStack itemStack = CraftItemStack.asNMSCopy(i);
        if (itemStack == null) return false;
        NBTTagCompound tag = itemStack.getTag();
        if (tag == null) return false;
        return tag.hasKey("BedWarsProxy");
    }

    @Override
    public String getCustomData(ItemStack i) {
        net.minecraft.server.v1_8_R3.ItemStack itemStack = CraftItemStack.asNMSCopy(i);
        NBTTagCompound tag = itemStack.getTag();
        if (tag == null) return "";
        return tag.getString("BedWarsProxy");
    }

    @Override
    public void registerCommand(Command command) {
        ((CraftServer) Bukkit.getServer()).getCommandMap().register(command.getName(), command);
    }
}
