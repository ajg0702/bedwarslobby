package com.andrei1058.bedwarslobby.language;

import com.andrei1058.bedwarslobby.config.ConfigPath;

public class Messages {
    public static String PREFIX = "prefix";

    /** General commands reply */
    public static String COMMAND_MAIN = "cmd-main-list";
    public static String COMMAND_LANG_LIST_HEADER = "cmd-lang-list-header";
    public static String COMMAND_LANG_LIST_FORMAT = "cmd-lang-list-format";
    public static String COMMAND_LANG_USAGE = "cmd-lang-usage";
    public static String COMMAND_LANG_SELECTED_NOT_EXIST = "cmd-lang-not-exist";
    public static String COMMAND_LANG_SELECTED_SUCCESSFULLY = "cmd-lang-set";
    public static String COMMAND_LANG_USAGE_DENIED = "cmd-lang-not-set";
    public static String COMMAND_JOIN_USAGE = "cmd-join-usage";
    public static String COMMAND_JOIN_GROUP_OR_ARENA_NOT_FOUND = "cmd-join-not-found";
    public static String COMMAND_JOIN_DENIED_IS_FULL = "cmd-join-arena-full";
    public static String COMMAND_JOIN_NO_EMPTY_FOUND = "cmd-join-arenas-full";
    public static String COMMAND_JOIN_DENIED_IS_FULL_OF_VIPS = "cmd-join-arena-full-vips";
    public static String COMMAND_JOIN_DENIED_PARTY_TOO_BIG = "cmd-join-party-big";
    public static String COMMAND_JOIN_DENIED_NOT_PARTY_LEADER = "cmd-join-not-leader";
    public static String COMMAND_JOIN_PLAYER_JOIN_MSG = "cmd-join-success";
    public static String COMMAND_JOIN_SPECTATOR_MSG = "cmd-join-spectate";
    public static String COMMAND_JOIN_SPECTATOR_DENIED_MSG = "cmd-join-spectate-denied";
    public static String REJOIN_NO_ARENA = "cmd-rejoin-no-arena";
    public static String REJOIN_DENIED = "cmd-rejoin-denied";
    public static String REJOIN_ALLOWED = "cmd-rejoin-allowed";
    public static String COMMAND_LEAVE_MSG = "cmd-leave";
    public static String COMMAND_PARTY_HELP = "cmd-party-help";
    public static String COMMAND_PARTY_INVITE_USAGE = "cmd-party-invite-usage";
    public static String COMMAND_PARTY_INVITE_SENT = "cmd-party-invite";
    public static String COMMAND_PARTY_INVITE_DENIED_PLAYER_OFFLINE = "cmd-party-offline";
    public static String COMMAND_PARTY_INVITE_SENT_TARGET_RECEIVE_MSG = "cmd-party-invite-received";
    public static String COMMAND_PARTY_INVITE_DENIED_CANNOT_INVITE_YOURSELF = "cmd-party-invite-self";
    public static String COMMAND_PARTY_ACCEPT_DENIED_NO_INVITE = "cmd-party-no-invite";
    public static String COMMAND_PARTY_ACCEPT_DENIED_ALREADY_IN_PARTY = "cmd-party-already-in";
    public static String COMMAND_PARTY_INSUFFICIENT_PERMISSIONS = "cmd-party-no-perm";
    public static String COMMAND_PARTY_ACCEPT_USAGE = "cmd-party-accept-usage";
    public static String COMMAND_PARTY_ACCEPT_SUCCESS = "cmd-party-join";
    public static String COMMAND_PARTY_GENERAL_DENIED_NOT_IN_PARTY = "cmd-party-not-in";
    public static String COMMAND_PARTY_LEAVE_DENIED_IS_OWNER_NEEDS_DISBAND = "cmd-party-cant-leave";
    public static String COMMAND_PARTY_LEAVE_SUCCESS = "cmd-party-leave";
    public static String COMMAND_PARTY_DISBAND_SUCCESS = "cmd-party-disband";
    public static String COMMAND_PARTY_REMOVE_USAGE = "cmd-party-remove-usage";
    public static String COMMAND_PARTY_REMOVE_SUCCESS = "cmd-party-remove";
    public static String COMMAND_PARTY_REMOVE_DENIED_TARGET_NOT_PARTY_MEMBER = "cmd-party-remove-not-in";
    public static String COMMAND_NOT_FOUND_OR_INSUFF_PERMS = "cmd-not-found";
    public static String COMMAND_FORCESTART_NOT_IN_GAME = "cmd-start-no-game";
    public static String COMMAND_FORCESTART_SUCCESS = "cmd-start";
    public static String COMMAND_FORCESTART_NO_PERM = "cmd-start-no-perm";
    public static String COMMAND_COOLDOWN = "cmd-cooldown";
    public static String COMMAND_SET_SPAWN_DISPLAY = "cmd-set-spawn-display";
    public static String COMMAND_SET_SPAWN_HOVER = "cmd-set-spawn-hover";
    public static String COMMAND_BUILD_DISPLAY = "cmd-build-display";
    public static String COMMAND_BUILD_HOVER = "cmd-build-hover";
    public static String COMMAND_GUI_DISPLAY = "cmd-arena-gui-display";
    public static String COMMAND_GUI_HOVER = "cmd-arena-gui-hover";
    public static String COMMAND_STATS_DISPLAY = "cmd-stats-display";
    public static String COMMAND_STATS_HOVER = "cmd-stats-hover";
    public static String COMMAND_LEVEL_DISPLAY = "cmd-level-display";
    public static String COMMAND_LEVEL_HOVER = "cmd-level-hover";
    public static String COMMAND_NPC_DISPLAY = "cmd-npc-display";
    public static String COMMAND_NPC_HOVER = "cmd-npc-hover";
    public static String COMMAND_LANGUAGE_DISPLAY = "cmd-lang-display";
    public static String COMMAND_LANGUAGE_HOVER = "cmd-lang-hover";

    /** Arena join/ leave related */
    public static String ARENA_JOIN_DENIED_SELECTOR = "arena-join-denied-selector";
    public static String ARENA_SPECTATE_DENIED_SELECTOR = "arena-spectate-denied-selector";
    public static String ARENA_LEAVE_PARTY_DISBANDED = "arena-leave-party-disbanded";

    /** Arena status/ status change related */
    public static String ARENA_STATUS_WAITING_NAME = "arena-status-waiting";
    public static String ARENA_STATUS_STARTING_NAME = "arena-status-starting";
    public static String ARENA_STATUS_PLAYING_NAME = "arena-status-playing";
    public static String ARENA_STATUS_RESTARTING_NAME = "arena-status-restarting";
    public static String ARENA_STATUS_OFFLINE_NAME = "arena-status-restarting";

    /** Arena GUI related */
    public static String ARENA_GUI_INV_NAME = "arena-selector-main-gui-name";
    public static String ARENA_GUI_ARENA_CONTENT_NAME = "arena-selector-main-content-name";
    public static String ARENA_GUI_ARENA_CONTENT_LORE = "arena-selector-main-content-lore";

    /** Stats related */
    public static String PLAYER_STATS_GUI_PATH = "stats";
    public static String PLAYER_STATS_GUI_INV_NAME = PLAYER_STATS_GUI_PATH+"-inv-name";

    /** General formatting */
    public static String FORMATTING_CHAT_LOBBY = "format-chat";
    public static String FORMATTING_SCOREBOARD_DATE = "format-sb-date";
    public static String FORMATTING_STATS_DATE_FORMAT = "format-stats-time";

    /** Meaning/ Translations */
    public static String MEANING_FULL = "meaning-full";
    public static String MEANING_NEVER = "meaning-never";
    public static String MEANING_NOBODY = "meaning-nobody";

    /** Scoreboard related */
    public static String SCOREBOARD_LOBBY = "scoreboard";

    /* Custom join items */
    public static final String GENERAL_CONFIGURATION_LOBBY_ITEMS_NAME = ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_PATH+"-%path%-name";
    public static final String GENERAL_CONFIGURATION_LOBBY_ITEMS_LORE = ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_PATH+"-%path%-lore";


}
